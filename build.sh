#!/bin/bash


replaces=''
conflicts=''
pkgversion=1.0
pkgname=drainware-c-icap
pkggroup=drainware
pkgarch=amd64
maintainer=jose.palanco@drainware.com
requires='monit,drainware-squid'
command="make install"



if [ $# -eq 1  ]
then
sudo checkinstall -y --replaces=$replaces --conflicts=$conflicts --nodoc --pkgversion=$pkgversion \
--pkgrelease=$1 --type=debian --pkgname=$pkgname --pkggroup=$pkggroup --pkgarch=$pkgarch \
--maintainer=$maintainer --requires=$requires --install=no  $command
mv *.deb ..


else
echo "usage: $0 [RELEASE]"
fi


